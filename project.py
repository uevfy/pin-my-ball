import pygame
from pathlib import Path
import numpy as np
import copy


# Initialize PyGame
pygame.init()

# Initial window size
s_width = 600
s_height = 800

# Define spacetime 
GRAVITY_X = 0.0
GRAVITY_Y = 0.03
DT = 1 # ms (discretization of time) 

collidecountdown = 0

# Making display screen
screen = pygame.display.set_mode((s_width, s_height), pygame.RESIZABLE)
bg_orig=pygame.image.load(Path(__file__).parents[0] / Path("bkg.jpg")).convert()


clock = pygame.time.Clock()

# Setup 
running = True


# important funcitons

def rotation_matrix(point,angle):
    return([point[0]*np.cos(angle)-point[1]*np.sin(angle),point[0]*np.sin(angle)+point[1]*np.cos(angle)])



  # def Linie(self):
    #     Linien = []
    #     for element in range(0,len(self.corners)):
            
    #         if element == len(self.corners)-1:
            
    #             P1 = self.corners[element]
    #             P2 = self.corners[0]
    #         else:
    #             P1 = self.corners[element]
    #             P2 = self.corners[element + 1]
            
            
    #         if P1[0] < P2[0]:
    #            Steigung = (P2[1]-P1[1])/(P2[0]-P1[0])
    #            Linien.append([P1,P2,Steigung])
    #         else:
    #            Steigung = (P1[1]-P2[1])/(P1[0]-P2[0]+0.01)
    #            Linien.append([P2,P1,Steigung])
        
    #     return(Linien)

        
    
    

# You could declare components (the initial ball, the other items, ...) here
class Ball:
    

    
    def __init__(self,x,y,vx,vy,Radius):
        self.position = [x,y]
        self.speed = [vx,vy]
        self.rad = Radius
        

class Polygon:
    
    def __init__(self,points,color,middle=0,rotationspeed=0,rotationdirection=0,dampening=1):
        self.color = pygame.Color(color)
        self.collidecountdown = 0
        self.dampening = dampening
        self.rotationspeed = rotationspeed
        self.rotationdirection = rotationdirection
        self.direction = 0
        if middle == 0:
          a = 0
          b = 0
          for element in points:
            a += element[0]
            b += element[1]
          self.middle = [a/len(points),b/len(points)]
        else:
            self.middle = middle
        self.start = copy.copy(self.middle)
          
        self.corners = []  
        for element in points:
            self.corners.append([element[0]-self.middle[0],element[1]-self.middle[1]])
        
        
    def draw(self):
        if self.collidecountdown > 0:
            color = [0,self.color[1],self.color[2]]
        else:
            color = self.color
        
        points = []
        for element in self.corners:
            points.append([element[0]+self.middle[0],element[1]+self.middle[1]])
        pygame.draw.polygon(screen, color ,points)
        

   
    def lines(self):
    
        line = []
        
    
    
        for element in range(0,len(self.corners)):
           P1 = self.corners[element-1]
           P2 = self.corners[element]
           
        
           lenght0 = np.sqrt((P1[0]-P2[0])**2+(P1[1]-P2[1])**2)
        
           angle = 0
        
           if P1[0]-P2[0] == 0:
               angle = -np.pi/2
           elif P1[0]-P2[0] < 0:
               angle = np.arctan((P2[1]-P1[1])/((P2[0]-P1[0])))
           else:
               angle = -np.pi + np.arctan((P1[1]-P2[1])/((P1[0]-P2[0])))
            
           line.append([P1,angle,lenght0])
        
    
        
        return(line)
    
    
    def collide(self):
        
        if self.collidecountdown <= 0:
            for element in self.lines():

                rel = rotation_matrix(element[0],-element[1])
                
                
                pos = rotation_matrix([ball.position[0]-self.middle[0],ball.position[1]-self.middle[1]],-element[1])
                
                
                if rel[0]-10 < pos[0]  < rel[0] + element[2] + 10 and rel[1]+10 > pos[1] > rel[1]-10:

                    g = 2*pos[0]*self.rotationspeed*self.rotationdirection
                    
                    ball.speed = rotation_matrix(ball.speed,-element[1])
                    
                    ball.speed = [ball.speed[0],-self.dampening*ball.speed[1]+g]
                    
                    ball.speed = rotation_matrix(ball.speed,element[1])
                    
                    self.collidecountdown = 10
                
                
            
    
    
        
    
    
    
    def rotate(self,angle):
        
        """
        phi: Rotation per Frame

        Das Objekt wird um seinen Mittelpunkt rotiert
        """
        
        self.corners = [rotation_matrix(element,angle) for element in self.corners]
    
    
        
    def push(self,phi,speed,length,dynamisch=0):
        
        """
        Das Objekt wird hin und her bewegt
         
        phi : Winkel der Auslenkung 
     
        speed : Geschwindigkeit
         
        length : Distanz der Verschiebung in beide Richtungen
        """
        
        dx = (self.middle[0]-self.start[0])**2
        
        dy = (self.middle[1]-self.start[1])**2
        
        k = dynamisch * abs((length**2 - dx -dy)/length**2)
        
        speed = speed + k


       
        if dx + dy > length**2:
            if self.direction == 0:
                self.direction = 1
            else:
                self.direction = 0
         
        if self.direction != 0:
            speed = -speed
            
        self.middle[0] += np.cos(phi)*speed 
        self.middle[1] += np.sin(phi)*speed
            
        
        
        
            
            
        

            

ball = Ball(400,100,0,3,20)

Hinderniss = Polygon([[300,300],[275,350],[300,400],[500,400],[500,300]],[100,100,100],0,0.01,1,0.8)

obstacle = Polygon([[100,100],[125,150],[100,200],[150,175],[200,200],[200,100]],[200,0,100],[300,400],0,1,0.8)





####
# Main event loop
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            # Tastaturtaste wurde gedrückt

            # Überprüfen, ob die Leertaste gedrückt wurde
            if event.key == pygame.K_SPACE:
                obstacle.rotationspeed = 0.01
                
           # if event.key == 
                
        elif event.type == pygame.KEYUP:
            # Tastaturtaste wurde losgelassen
            obstacle.rotationspeed = -0.01
        continue
    
    collidecountdown -= 1


   
    # Adjust screen
    s_width, s_height = screen.get_width(), screen.get_height()
    bg = pygame.transform.scale(bg_orig, (s_width, s_height))
    screen.blit(bg, (0, 0)) # redraws background image

    # Here the action could take place

    # s = s0 + v0*t + 1/2a*t**2
   # for element in Hinderniss.lines():
   #     pygame.draw.line(screen, (100,100,100),element[0]+Hinderniss.middle, element[0]*Hinderniss., width=10)
  
    Hinderniss.draw()
    
    obstacle.draw()
    
    obstacle.collide()
    obstacle.collidecountdown -= 1
    
    Hinderniss.collide()
    Hinderniss.collidecountdown -= 1
    
    

  

    ball.speed[1] += GRAVITY_Y*DT

    if ball.position[1] >= screen.get_height():
        ball.speed[1] = ball.speed[1] * (-1)
    if ball.position[1] <= 0:
        ball.speed[1] = ball.speed[1] * (-1)
    if ball.position[0]>= 600 or ball.position[0] <=0 :
        ball.speed[0] = ball.speed[0] * (-1)

    ball.position[1] += ball.speed[1]*DT + 0.5 * GRAVITY_Y*DT**2
    ball.position[0] = ball.position[0] + ball.speed[0]*DT

    pygame.draw.circle(screen, (35, 161, 224), ball.position , ball.rad)


    Hinderniss.rotate(Hinderniss.rotationspeed)
    

    obstacle.rotate(obstacle.rotationspeed)
    
    #print(np.sqrt(ball.speed[0]**2+ball.speed[1]**2))
    
    #Hinderniss.push(np.pi/2,0.1,200,0.4)


    #Dann ballern wir es mal hier rein....:
   #if ball.x is 
   
  

    
    pygame.display.flip() # Update the display of the full screen
    clock.tick(120) # 120 frames per second

# Done! Time to quit.